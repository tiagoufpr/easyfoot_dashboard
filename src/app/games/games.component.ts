import {
  Component,
  OnInit,
  ViewChild,
  HostListener,
  AfterViewInit,
  ChangeDetectorRef
} from "@angular/core";

import { Game } from "../../mock/game";
import { GameService } from "../service/game.service";
import {
  MdbTablePaginationComponent,
  MdbTableService
} from "angular-bootstrap-md";

@Component({
  selector: "app-games",
  templateUrl: "./games.component.html",
  styleUrls: ["./games.component.css"]
})
export class GamesComponent implements OnInit, AfterViewInit {
  @ViewChild(MdbTablePaginationComponent)
  mdbTablePagination: MdbTablePaginationComponent;
  games: Game[];

  elements: any = [];
  previous: any = [];
  headElements = ["Id", "Nome", "Lugar", "Criador", "Data", "Opções"];

  firstItemIndex;
  lastItemIndex;
  searchText: string = "";

  constructor(
    private gameService: GameService,
    private tableService: MdbTableService,
    private cdRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.getGames();

    for (let i = 1; i <= 15; i++) {
      this.elements.push({
        id: i.toString(),
        name: "Nome" + i,
        place: "Lugar" + i,
        date: "Data" + i,
        createdBy: "Criador" + i,
        handle: "Opções" + i
      });
    }

    this.tableService.setDataSource(this.elements);
    this.elements = this.tableService.getDataSource();
    this.previous = this.tableService.getDataSource();
  }

  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(5);
    this.firstItemIndex = this.mdbTablePagination.firstItemIndex;
    this.lastItemIndex = this.mdbTablePagination.lastItemIndex;

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  onNextPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

  onPreviousPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

  getGames(): void {
    this.gameService.getGames().subscribe(games => (this.games = games));
  }

  delete(game: Game): void {
    this.games = this.games.filter(p => p !== game);
    this.gameService.deleteGame(game).subscribe();
  }
}
