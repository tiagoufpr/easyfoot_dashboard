import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { AuthService } from "../service/auth.service";

@Component({
  selector: "app-auth",
  templateUrl: "./auth.component.html",
  styleUrls: ["./auth.component.css"]
})
export class AuthComponent implements OnInit {
  loginForm: FormGroup;
  @ViewChild("loginInput")
  loginInput: ElementRef<HTMLInputElement>;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      login: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  login() {
    const login = this.loginForm.get("login").value;
    const password = this.loginForm.get("password").value;

    this.authService.authenticate(login, password).subscribe(
      () => this.router.navigateByUrl("/players"),
      err => {
        console.log(login, password);
        console.log(err);
        this.loginForm.reset();
        this.loginInput.nativeElement.focus();
        alert("Usuário ou senha Inválido!");
      }
    );
  }
}
