import {
  Component,
  OnInit,
  ViewChild,
  HostListener,
  AfterViewInit,
  ChangeDetectorRef
} from "@angular/core";

import { Team } from "../../mock/team";
import { TeamService } from "../service/team.service";
import {
  MdbTablePaginationComponent,
  MdbTableService
} from "angular-bootstrap-md";

@Component({
  selector: "app-teams",
  templateUrl: "./teams.component.html",
  styleUrls: ["./teams.component.css"]
})
export class TeamsComponent implements OnInit, AfterViewInit {
  @ViewChild(MdbTablePaginationComponent)
  mdbTablePagination: MdbTablePaginationComponent;

  elements: any = [];
  previous: any = [];
  headElements = ["Id", "Nome", "Capitão", "Opções"];

  firstItemIndex;
  lastItemIndex;
  searchText: string = "";

  teams: Team[];

  constructor(
    private teamService: TeamService,
    private tableService: MdbTableService,
    private cdRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.getTeams();

    for (let i = 1; i <= 15; i++) {
      this.elements.push({
        id: i.toString(),
        name: "Nome" + i,
        captain: "Copitão" + i,
        handle: "Opções " + i
      });
    }

    this.tableService.setDataSource(this.elements);
    this.elements = this.tableService.getDataSource();
    this.previous = this.tableService.getDataSource();
  }

  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(5);
    this.firstItemIndex = this.mdbTablePagination.firstItemIndex;
    this.lastItemIndex = this.mdbTablePagination.lastItemIndex;

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  onNextPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

  onPreviousPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

  getTeams(): void {
    this.teamService.getTeams().subscribe(players => (this.teams = players));
  }

  delete(team: Team): void {
    this.teams = this.teams.filter(t => t !== team);
    this.teamService.deleteTeam(team.id).subscribe();
  }
}
