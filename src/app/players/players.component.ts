import {
  Component,
  OnInit,
  ViewChild,
  HostListener,
  AfterViewInit,
  ChangeDetectorRef
} from "@angular/core";

import { Player } from "../../mock/player";
import { PlayerService } from "../service/player.service";
import {
  MdbTablePaginationComponent,
  MdbTableService
} from "angular-bootstrap-md";

@Component({
  selector: "app-players",
  templateUrl: "./players.component.html",
  styleUrls: ["./players.component.css"]
})
export class PlayersComponent implements OnInit, AfterViewInit {
  @ViewChild(MdbTablePaginationComponent)
  mdbTablePagination: MdbTablePaginationComponent;
  players: Player[];

  elements: any = [];
  previous: any = [];
  headElements = ["Id", "Nome", "E-mail", "Opções"];

  firstItemIndex;
  lastItemIndex;
  searchText: string = "";

  constructor(
    private playerService: PlayerService,
    private tableService: MdbTableService,
    private cdRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.getPlayers();

    for (let i = 1; i <= 15; i++) {
      this.elements.push({
        id: i.toString(),
        name: "Nome" + i,
        email: "E-mail " + i,
        handle: "Opções" + i
      });
    }

    this.tableService.setDataSource(this.elements);
    this.elements = this.tableService.getDataSource();
    this.previous = this.tableService.getDataSource();
  }

  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(5);
    this.firstItemIndex = this.mdbTablePagination.firstItemIndex;
    this.lastItemIndex = this.mdbTablePagination.lastItemIndex;

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  onNextPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

  onPreviousPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

  getPlayers(): void {
    this.playerService
      .getPlayers()
      .subscribe(players => (this.players = players));
  }

  add(name: string): void {
    name = name.trim();
    if (!name) {
      return;
    }
    this.playerService.addPlayer({ name } as Player).subscribe(player => {
      this.players.push(player);
    });
  }

  delete(player: Player): void {
    this.players = this.players.filter(p => p !== player);
    this.playerService.deletePlayer(player.id).subscribe();
  }
}
