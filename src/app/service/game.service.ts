import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { catchError, map, tap } from "rxjs/operators";
const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

import { Game } from "../../mock/game";
import { MessageService } from "./message.service";

@Injectable({
  providedIn: "root"
})
export class GameService {
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  private gameURL = "http://localhost:8080/game"; // URL to web api
  private registrationURL = "http://localhost:8080/registration"; // URL to web api

  /** Log a gameServiceService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`gameService: ${message}`);
  }

  /** GET games from the server */
  getGames(): Observable<Game[]> {
    return this.http.get<Game[]>(this.gameURL + "/getAll").pipe(
      tap(games => this.log("fetched games")),
      catchError(this.handleError("getGames", []))
    );
  }

  /** GET game by id. Will 404 if id not found */
  getGame(id: number): Observable<Game> {
    const url = `${this.gameURL}/` + "get/" + `${id}`;
    return this.http.get<Game>(url).pipe(
      tap(_ => this.log(`fetched game id=${id}`)),
      catchError(this.handleError<Game>(`getGame id=${id}`))
    );
  }

  /** PUT: update the game on the server */
  updateGame(game: Game): Observable<any> {
    return this.http
      .put<Game>(this.gameURL + "/update/" + `${game.id}`, game, httpOptions)
      .pipe(
        tap(_ => this.log(`updated game id=${game.id}`)),
        catchError(this.handleError<any>("updateGame"))
      );
  }

  
  /** DELETE: delete the game from the server */
  deleteGame(game: Game | number): Observable<Game> {
    const id = typeof game === "number" ? game : game.id;
    const url = `${this.registrationURL}/deleteandgame/${id}`;

    return this.http.delete<any>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted registration game id=${id}`)),
      catchError(this.handleError<Game>("deleteGame"))
    );
  }


  /** POST: add a new game to the server */
  // addGame(game: Game): Observable<Game> {
  //   return this.http
  //     .post<Game>(this.gameURL + "/create", game, httpOptions)
  //     .pipe(
  //     tap((game: Game) =>
  //         this.log(`added game w/ id=${game.id}`)
  //       ),
  //     catchError(this.handleError<Game>("addGame"))
  //     );
  // }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
