import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { catchError, map, tap } from "rxjs/operators";
const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

import { Team } from "../../mock/team";
import { MessageService } from "./message.service";

@Injectable({
  providedIn: "root"
})
export class TeamService {
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  private teamsUrl = "http://localhost:8080/team"; // URL to web api

  /** Log a TeamServiceService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`TeamService: ${message}`);
  }

  /** GET teams from the server */
  getTeams(): Observable<Team[]> {
    return this.http.get<Team[]>(this.teamsUrl + "/getAll").pipe(
      tap(teams => this.log("fetched teams")),
      catchError(this.handleError("getTeams", []))
    );
  }

  /** GET Team by id. Will 404 if id not found */
  getTeam(id: number): Observable<Team> {
    const url = `${this.teamsUrl}/` + "get/" + `${id}`;
    return this.http.get<Team>(url).pipe(
      tap(_ => this.log(`fetched team id=${id}`)),
      catchError(this.handleError<Team>(`getTeam id=${id}`))
    );
  }

  /** PUT: update the team on the server */
  updateTeam(team: Team): Observable<any> {
    return this.http
      .put<Team>(this.teamsUrl + "/update/" + `${team.id}`, team, httpOptions)
      .pipe(
        tap(_ => this.log(`updated team id=${team.id}`)),
        catchError(this.handleError<any>("updateTeam"))
      );
  }

  /** DELETE: delete the team from the server */
  deleteTeam(team: Team | number): Observable<Team> {
    const id = typeof team === "number" ? team : team.id;
    const url = `${this.teamsUrl}/delete/${id}`;

    return this.http.delete<Team>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted team id=${id}`)),
      catchError(this.handleError<Team>("deleteTeam"))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
