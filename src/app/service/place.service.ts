import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { catchError, map, tap } from "rxjs/operators";
const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

import { Place } from "../../mock/place";
import { MessageService } from "./message.service";

@Injectable({
  providedIn: "root"
})
export class PlaceService {
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  private placeURL = "http://localhost:8080/place"; // URL to web api

  /** Log a placeService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`placeService: ${message}`);
  }

  /** GET places from the server */
  getPlaces(): Observable<Place[]> {
    return this.http.get<Place[]>(this.placeURL + "/getAll").pipe(
      tap(places => this.log("fetched places")),
      catchError(this.handleError("getPlaces", []))
    );
  }

  /** GET place by id. Will 404 if id not found */
  getPlace(id: number): Observable<Place> {
    const url = `${this.placeURL}/` + "get/" + `${id}`;
    return this.http.get<Place>(url).pipe(
      tap(_ => this.log(`fetched place id=${id}`)),
      catchError(this.handleError<Place>(`getPlace id=${id}`))
    );
  }

  /** PUT: update the place on the server */
  updatePlace(place: Place): Observable<any> {
    return this.http
      .put<Place>(this.placeURL + "/updatePlace/" + `${place.id}`, place, httpOptions)
      .pipe(
        tap(_ => this.log(`updated place id=${place.id}`)),
        catchError(this.handleError<any>("updatePlace"))
      );
  }

  
  /** DELETE: delete the place from the server */
  deletePlace(place: Place | number): Observable<Place> {
    const id = typeof place === "number" ? place : place.id;
    const url = `${this.placeURL}/deletePlace/${id}`;

    return this.http.delete<any>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted place id=${id}`)),
      catchError(this.handleError<Place>("deletePlace"))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
