import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { catchError, map, tap } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(private http: HttpClient) {}

  authenticate(login: string, password: string) {
    const API_URL = `http://localhost:8080/admin/login?login=${login}&password=${password}`;

    return this.http
      .post(API_URL, { login, password }, { observe: "response" })
      .pipe(
        tap(res => {
          const authToken = res.headers.get("x-access-token");
          console.log(`User ${login} authenticated with token ${authToken}`);
        })
      );
  }
}
