import {
  Component,
  OnInit,
  ViewChild,
  HostListener,
  AfterViewInit,
  ChangeDetectorRef
} from "@angular/core";

import { Place } from "../../mock/place";
import { PlaceService } from "../service/place.service";
import {
  MdbTablePaginationComponent,
  MdbTableService
} from "angular-bootstrap-md";

@Component({
  selector: "app-places",
  templateUrl: "./places.component.html",
  styleUrls: ["./places.component.css"]
})
export class PlacesComponent implements OnInit, AfterViewInit {
  @ViewChild(MdbTablePaginationComponent)
  mdbTablePagination: MdbTablePaginationComponent;

  elements: any = [];
  previous: any = [];
  headElements = ["Id", "Nome", "Opções"];

  firstItemIndex;
  lastItemIndex;
  searchText: string = "";

  places: Place[];

  constructor(
    private placeService: PlaceService,
    private tableService: MdbTableService,
    private cdRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.getPlaces();

    for (let i = 1; i <= 15; i++) {
      this.elements.push({
        id: i.toString(),
        name: "Nome" + i,
        handle: "Opções " + i
      });
    }

    this.tableService.setDataSource(this.elements);
    this.elements = this.tableService.getDataSource();
    this.previous = this.tableService.getDataSource();
  }

  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(5);
    this.firstItemIndex = this.mdbTablePagination.firstItemIndex;
    this.lastItemIndex = this.mdbTablePagination.lastItemIndex;

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  onNextPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

  onPreviousPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

  getPlaces(): void {
    this.placeService.getPlaces().subscribe(places => (this.places = places));
  }

  delete(place: Place): void {
    this.places = this.places.filter(t => t !== place);
    this.placeService.deletePlace(place.id).subscribe();
  }
}
