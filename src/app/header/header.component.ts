import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit {
  title = "EasyFoot Admin";
  players = "Jogadores";
  teams = "Times";
  games = "Partidas";
  places = "Lugares";

  constructor() {}

  ngOnInit() {}
}
