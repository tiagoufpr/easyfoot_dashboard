import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { PlayersComponent } from "./players/players.component";
import { PlayerDetailComponent } from "./player-detail/player-detail.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { TeamsComponent } from "./teams/teams.component";
import { TeamDetailComponent } from "./team-detail/team-detail.component";
import { GamesComponent } from "./games/games.component";
import { GameDetailComponent } from "./game-detail/game-detail.component";
import { PlacesComponent } from "./places/places.component";
import { PlaceDetailComponent } from "./place-detail/place-detail.component";
import { AuthComponent } from "./auth/auth.component";

const routes: Routes = [
  { path: "", redirectTo: "/auth", pathMatch: "full" },
  { path: "dashboard", component: DashboardComponent },
  { path: "player-detail/:id", component: PlayerDetailComponent },
  { path: "players", component: PlayersComponent },
  { path: "teams", component: TeamsComponent },
  { path: "team-detail/:id", component: TeamDetailComponent },
  { path: "games", component: GamesComponent },
  { path: "game-detail/:id", component: GameDetailComponent },
  { path: "places", component: PlacesComponent },
  { path: "place-detail/:id", component: PlaceDetailComponent },
  { path: "auth", component: AuthComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
