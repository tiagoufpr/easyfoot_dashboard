import { Component, OnInit, Input } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";

import { Place } from "../../mock/place";
import { PlaceService } from "../service/place.service";

@Component({
  selector: "app-place-detail",
  templateUrl: "./place-detail.component.html",
  styleUrls: ["./place-detail.component.css"]
})
export class PlaceDetailComponent implements OnInit {
  place: Place;

  constructor(
    private route: ActivatedRoute,
    private placeService: PlaceService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getPlace();
  }

  getPlace(): void {
    const id = +this.route.snapshot.paramMap.get("id");
    this.placeService.getPlace(id).subscribe(place => (this.place = place));
  }

  save(): void {
    this.placeService.updatePlace(this.place).subscribe(() => this.goBack());
    console.log(this.place);
  }

  goBack(): void {
    this.location.back();
  }
}
