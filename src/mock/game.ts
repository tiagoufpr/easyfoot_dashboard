import { Player } from "./player";
import { Team } from "./team";

export class Game {
  id: number;
  name: string;
  dateTime: Date;
  createdBy: Player;
  place: {id: number, name: string; city: {id:number, name: string, state:{id: number, name: string, country:{id: number, name: string}}}, latitude: number, longitude: number };
  teams: [{ team: Team }];
}
