import { Player } from "./player";
import { Game } from "./game";

export class Team {
  id: number;
  name: string;
  city: { id: number; name: string };
  captain: Player;
  coCaptain: Player;
  players: [
    {
      player: Player;
    }
  ];
  games: [
    {
      game: Game;
    }
  ];
}
