export class Place {
  id: number;
  name: string;
  city: { id: number; name: string };
  latitude: number;
  longitude: number;
}
